
// structure draft :

export interface JsonFormsState {
  jsonforms: {
    core: {
      data: any;
      schema: JsonSchema;
      uischema: UISchemaElement;
      errors?: ErrorObject[];
      validator?: ValidateFunction;
      ajv?: Ajv;
      refParserOptions?: RefParser.Options;
    };
    config?: any;
    renderers?: JsonFormsRendererRegistryEntry[];
    cells?: JsonFormsCellRendererRegistryEntry[];
    // allow additional state for JSON Forms
    [additionalState: string]: any;
  };
}

export const JSONForm = ({ data = {}, schema = {}, uiSchema } = {}) => {}

export default JSONForm
